﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Apoteka
{
	public class Lek
	{
		public int Sifra { get; set; }
		public string Naziv { get; set; }
		public double Cena { get; set; }
		public int Kolicina { get; set; }

		public Lek(int sifra, string naziv)
		{
			Sifra = sifra;
			Naziv = naziv;
		}
		public Lek(int sifra, string naziv, double cena, int kolicina)
		{
			Sifra = sifra;
			Naziv = naziv;
			Cena = cena;
			Kolicina = kolicina;
		}
		public Lek(int sifra, string naziv, double cena)
		{
			Sifra = sifra;
			Naziv = naziv;
			Cena = cena;
		}
	}
}
