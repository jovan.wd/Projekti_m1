﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Apoteka
{
	public class Dobavljac
	{
		public int DobavljacId { get; set; }
		public string Naziv { get; set; }
		public string Adresa { get; set; }
		public string Mesto { get; set; }
		public int Broj { get; set; }

		public Dobavljac(int id, string naziv, string adresa, string mesto, int broj)
		{
			DobavljacId = id;
			Naziv = naziv;
			Adresa = adresa;
			Mesto = mesto;
			Broj = broj;
		}
	}
}
