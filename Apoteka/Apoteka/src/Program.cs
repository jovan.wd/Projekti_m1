﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Apoteka
{
	class Program
	{
		static void Main(string[] args)
		{
			List<Lek> lekovi = new List<Lek>();
			double ukupno=0;
			Apotekar ap1 = new Apotekar(1, "Nenad", "Nenadic", 0655554432);
			Apotekar ap2 = new Apotekar(2, "Sinisa", "Sinisic", 063222555);

			Lek lek1 = new Lek(1111, "Andol", 115.99, 100);
			Lek lek2 = new Lek(2222, "Brufen", 222.50, 50);
			Lek lek3 = new Lek(3332, "Ranisan", 297.78, 20);
			lekovi.Add(lek1); lekovi.Add(lek2); lekovi.Add(lek3);
			foreach(Lek lek in lekovi)
			{
				ukupno += lek.Cena * lek.Kolicina;
			}
			Dobavljac dob = new Dobavljac(123, "LillyApoteke", "Bulevar Oslobodjenja 30", "Novi Sad", 0216023589);

			Narudzbenica narudzba = new Narudzbenica(ap1, dob, "18/05/2017",ukupno, ap2);
			narudzba.ListaLekova = lekovi;
			Console.WriteLine(narudzba.IspisiPotpunuTekstualnuReprezentacijuKlase());
            Console.WriteLine("**************************************************************************************************\nUkupno:\t"+ ukupno+ " din.");
			Console.ReadKey();
		}
	}
}
