﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Apoteka
{
	public class Narudzbenica
	{
		public Apotekar ApotekarKreator { get; set; }
		public Dobavljac Dobavljac { get; set; }
		public string Datum { get; set; }
		public double UkupnaCena { get; set; }
		public Apotekar ApotekarNarucilac { get; set; }
		public List<Lek> ListaLekova;

		public Narudzbenica()
		{
			ListaLekova = new List<Lek>();
		}

		public Narudzbenica(Apotekar kreator)
		{
			ApotekarKreator = kreator;
		}
		public Narudzbenica(Apotekar kreator, Dobavljac dobavljac, string datum, double ukupnacena, Apotekar narucilac)
		{
			ApotekarKreator = kreator;
			Dobavljac = dobavljac;
			Datum = datum;
			UkupnaCena = ukupnacena;
			ApotekarNarucilac = narucilac;
		}
		public string IspisiTekstualnuReprezentacijuKlase()
		{
			return "Apotekar " + ApotekarKreator.Ime + " " + ApotekarKreator.Prezime + " kreira narudzbenicu lekova " +
						Environment.NewLine + "Datum: " + Datum + " ukupna cena: " + UkupnaCena + " din.";
		}
		public string IspisiPotpunuTekstualnuReprezentacijuKlase()
		{
			string s = "Apotekar " + ApotekarKreator.Ime + " " + ApotekarKreator.Prezime + " kreira narudzbenicu lekova " +
						Environment.NewLine + "Datum: " + Datum + " ukupna cena: " + UkupnaCena + " din.\n";
			StringBuilder sb = new StringBuilder();
			sb.Append("Lista lekova:\n");
			foreach (Lek lek in ListaLekova)
			{
				sb.AppendLine("\t" + lek.Naziv + "\t" + lek.Kolicina + " kom\t" + lek.Cena + "din/Kom\t ukupno = " + lek.Kolicina * lek.Cena + "din.");
			}

			return s + sb.ToString();
		}
	}
}
