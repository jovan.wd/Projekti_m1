﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Apoteka
{
	public class Apotekar
	{
		public int ApotekarId { get; set; }
		public string Ime { get; set; }
		public string Prezime { get; set; }
		public int BrojTelefona { get; set; }

		public Apotekar(int id, string ime, string prezime, int broj)
		{
			ApotekarId = id;
			Ime = ime;
			Prezime = prezime;
			BrojTelefona = broj;
		}
	}
}
