﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StudentskaSluzbaSaDijagrama.src
{
	class Predmeti
	{
		public int PredmetId { get; set; }
		public string Naziv { get; set; }
		public List<Studenti> ListaStudenata { get; set; }

		public Predmeti()
		{

		}
		public Predmeti(int id)
		{
			PredmetId = id;
		}
		public Predmeti(int id, string naziv)
		{
			PredmetId = id;
			Naziv = naziv;
		}
	}
}
