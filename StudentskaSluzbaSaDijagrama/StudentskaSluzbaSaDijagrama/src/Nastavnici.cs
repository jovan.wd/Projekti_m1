﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StudentskaSluzbaSaDijagrama.src
{
	class Nastavnici
	{
		public int NastavnikId { get; set; }
		public string Ime { get; set; }
		public string Prezime { get; set; }
		public string Zvanje { get; set; }

		public Nastavnici()
		{
				
		}
		public Nastavnici(int id, string ime, string prezime, string zvanje)
		{
			NastavnikId = id;
			Ime = ime;
			Prezime = prezime;
			Zvanje = zvanje;
		}
	}
}
