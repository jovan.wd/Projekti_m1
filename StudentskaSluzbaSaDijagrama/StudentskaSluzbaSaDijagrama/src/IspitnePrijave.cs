﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StudentskaSluzbaSaDijagrama.src
{
	class IspitnePrijave
	{
		public Studenti StudentId { get; set; }
		public Predmeti PredmetId { get; set; }
		public IspitniRokovi RokId { get; set; }
		public int Teorija { get; set; }
		public int Zadaci { get; set; }

		public IspitnePrijave()
		{

		}
		public IspitnePrijave(Studenti studentid, Predmeti predmetid, IspitniRokovi irid, int teorija, int zadaci)
		{
			StudentId = studentid;
			PredmetId = predmetid;
			RokId = irid;
			Teorija = teorija;
			Zadaci = zadaci;
		}
	}
}
