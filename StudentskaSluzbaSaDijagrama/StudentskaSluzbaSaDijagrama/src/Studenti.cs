﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StudentskaSluzbaSaDijagrama.src
{
	class Studenti
	{
		public int StudentId { get; set; }
		public string Index { get; set; }
		public string Ime { get; set; }
		public string Prezime { get; set; }
		public string Grad { get; set; }

		public List<Predmeti> ListaPredmeta { get; set; }
		public Studenti()
		{

		}
		public Studenti(int id)
		{
			StudentId = id;
		}
		public Studenti(string index, string ime, string prezime)
		{
			Index = index;
			Ime = ime;
			Prezime = prezime;
		}
		public Studenti(string index, string ime, string prezime, string grad)
		{
			Index = index;
			Ime = ime;
			Prezime = prezime;
			Grad = grad;
		}
		public Studenti(int id, string index, string ime, string prezime, string grad)
		{
			StudentId = id;
			Index = index;
			Ime = ime;
			Prezime = prezime;
			Grad = grad;
		}
		public string PrikaziTekstualnuReprezentacijuKlase()
		{
			string student = "Student sa br. Indexa: "+Index+" zove se: "+Ime+" "+ Prezime+ ", zivi u: "+ Grad;
			return student;
		}
	}
}
