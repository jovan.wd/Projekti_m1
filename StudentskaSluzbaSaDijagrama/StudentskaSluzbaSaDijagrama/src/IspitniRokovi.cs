﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StudentskaSluzbaSaDijagrama.src
{
	class IspitniRokovi
	{
		private int IspitniRokId;
		private string Naziv;
		private string PocetakRoka;
		private string KrajRoka;

		public string Kraj
		{
			get { return KrajRoka; }
			set { KrajRoka = value; }
		}

		public string Pocetak
		{
			get { return PocetakRoka; }
			set { PocetakRoka = value; }
		}


		public string NazivRoka
		{
			get { return Naziv; }
			set { Naziv = value; }
		}


		public int RokId
		{
			get { return IspitniRokId; }
			set { IspitniRokId = value; }
		}


		public IspitniRokovi()
		{

		}
		public IspitniRokovi(int id, string naziv, string pocetak, string kraj)
		{
			IspitniRokId = id;
			Naziv = naziv;
			PocetakRoka = pocetak;
			KrajRoka = kraj;
		}
		public IspitniRokovi(string naziv, string pocetak, string kraj)
		{
			Naziv = naziv;
			PocetakRoka = pocetak;
			KrajRoka = kraj;
		}
		public IspitniRokovi(int id, string naziv)
		{
			IspitniRokId = id;
			Naziv = naziv;
		}
		public IspitniRokovi(int id)
		{
			RokId = id;
		}
		//public static bool Isti(List<IspitniRokovi> ir)
		//{
		//	for(int i = 0; i< ir.Count-1; i++)
		//	{
		//		if (ir[i].Equals(ir[i + 1]))
		//			return true;
		//		else return false;
		//	}
			
		//}
	}
}
