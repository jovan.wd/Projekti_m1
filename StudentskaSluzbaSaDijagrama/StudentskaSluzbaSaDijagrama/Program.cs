﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using StudentskaSluzbaSaDijagrama.src;

namespace StudentskaSluzbaSaDijagrama
{
	class Program
	{
		static void Main(string[] args)
		{
			string nastavniciTxt = "1,Petar,Petrović,Docent\n2,Jovan,Jovanović,Docent\n3,Marko,Marković,Asistent\n4,Nikola,Nikolić,Redovni Profesor\n5,Lazar,Lazić,Asistent";
			string predmetiTxt = "1,Matematika\n2,Fizika\n3,Elektrotehnika\n4,Informatika";
			string studentiTxt = "1,E1 01/2016,Jovanović,Zarko,Loznica\n2,E2 02/2015,Prosinečki,Strahinja,Novi Sad\n" +
				"3,E2 33/2016,Savić,Nebojša,Inđija\n4,SW 36/2013,Sekulić,Ana,Niš\n5,E2 157/2013,Nedeljković,Vuk,Novi Sad\n" +
				"6,E1 183/2013,Klainić,Jovana,Sombor\n7,E2 44/2015,Bojana,Panić,Sr.Mitrovica";
			string rokoviTxt = "1,Januarski,2015-01-15,2015-01-29\n2,Februarski,2015-02-01,2015-02-14";
			string prijaveTxt = "1,1,1,88,89\n1,2,2,85,55\n2,2,2,80,45\n3,1,1,94,96\n4,1,1,40,60\n4,1,2,83,88\n4,2,2,89,91\n4,4,2,100,98\n5,2,1,45,47\n5,3,2,56,55\n6,3,2,25,0";

			List<Predmeti> predmeti = new List<Predmeti>();
			List<Nastavnici> nastavnici = new List<Nastavnici>();
			List<Studenti> studenti= new List<Studenti>();
			List<IspitniRokovi> rokovi = new List<IspitniRokovi>();
			List<IspitnePrijave> prijave = new List<IspitnePrijave>();
			// Nastavnici
			UcitajNastavnike(nastavniciTxt, nastavnici);
			IspisiNastavnike(nastavnici);

			int idNastavnika = 1;
			IspisiNastavnikaPoIdentifikatoru(idNastavnika, nastavnici);

			// Predmeti
			UcitajPredmete(predmetiTxt, predmeti);
			IspisiPredmete(predmeti);

			int idPredmeta = 2;
			IspisiPredmetePoIdentifikatoru(idPredmeta, predmeti);

			// Studenti
			
			UcitajStudente(studentiTxt, studenti);
			IspisiStudente(studenti);
			int idStudenta = 3;
			IspisiStudentePoIdentifikatoru(idStudenta, studenti);

			IspisiStudenteSaSmera(studenti, "E2");
			IspisiStatistikuUpisa(studenti);

			// Ispitni Rokovi
			UcitajIspitneRokove(rokoviTxt, rokovi);
			Console.WriteLine(IspisiTekstualnuReprezentacijuKlase(rokovi));

			// Ispitne Prijave 

			UcitajPrijave(prijaveTxt, prijave);
			Console.WriteLine(IspisiTekstualnuReprezentacijuKlase(prijave));
			Console.ReadKey();
		}

		//Metorde Prijave Ispita

		private static void UcitajPrijave(string prijaveTxt, List<IspitnePrijave> prijave)
		{
			string[] pri = prijaveTxt.Split('\n');

			// 0 - student, 1 - predmet, 2 - rok, 3 - teorija, 4 - zadaci

			for (int i = 0; i < pri.Length; i++)
			{
				string[] podaciPrijave = pri[i].Split(',');
				prijave.Add(new IspitnePrijave());

				//new Studenti(int.Parse(podaciPrijave[0])), new Predmeti(int.Parse(podaciPrijave[1])), new IspitniRokovi(int.Parse(podaciPrijave[2])), int.Parse(podaciPrijave[3]), int.Parse(podaciPrijave[4]))
			}
		}
		private static StringBuilder IspisiTekstualnuReprezentacijuKlase(List<IspitnePrijave> prijave)
		{
			StringBuilder sb = new StringBuilder();
			sb.Append("Ispitne Prijave:\n");
			for (int i = 0; i < prijave.Count; i++)
			{
				string str = "Stident sa ID: "+prijave[i].StudentId+" predmet ID: "+prijave[i].PredmetId+" u roku: "+ prijave[i].RokId+" ima iz teorije "+prijave[i].Teorija+" a iz zadataka "+ prijave[i].Zadaci+" bodova\n";
				sb.Append(str);
			}
			return sb;
		}

		// Metode za nastavnika
		private static void UcitajNastavnike(string nastavniciTxt, List<Nastavnici> nastavnici)
		{
			string[] nast = nastavniciTxt.Split('\n');
			
			 // 0 - identifikatori, 1 - imena, 2 - prezimena, 3 - zvanja

			for (int i = 0; i < nast.Length; i++)
			{
				string[] podaciNastavnika = nast[i].Split(',');
				nastavnici.Add(new Nastavnici(Int32.Parse(podaciNastavnika[0]), podaciNastavnika[1], podaciNastavnika[2], podaciNastavnika[3]));
			}
		}
		private static void IspisiNastavnike(List<Nastavnici> nastavnici)
		{
			Console.WriteLine("Nastavici:");
			for (int i = 0; i < nastavnici.Count; i++)
			{
				Console.WriteLine($"id[{nastavnici[i].NastavnikId}] Nastavnik {nastavnici[i].Ime,-15} {nastavnici[i].Prezime,-15} je u zvanju {nastavnici[i].Zvanje}");
			}
		}

		private static void UcitajIspitneRokove(string rokoviTxt, List<IspitniRokovi> rokovi)
		{
			string[] rok = rokoviTxt.Split('\n');

			// 0 - identifikatori, 1 - naziv, 2 - pocetak, 3 - kraj

			for (int i = 0; i < rok.Length; i++)
			{
				string[] podaciRoka = rok[i].Split(',');
				rokovi.Add(new IspitniRokovi(Int32.Parse(podaciRoka[0]), podaciRoka[1], podaciRoka[2], podaciRoka[3]));
			}
		}
		private static StringBuilder IspisiTekstualnuReprezentacijuKlase(List<IspitniRokovi> rokovi)
		{
			StringBuilder sb = new StringBuilder();
			sb.Append("Ispitni Rokovi:\n");
			for (int i = 0; i < rokovi.Count; i++)
			{
				string str = "id[" + rokovi[i].RokId + "] Naziv roka " + rokovi[i].NazivRoka + " pocetak roka " + rokovi[i].Pocetak +"\n";
				sb.Append(str);
			}
			return sb;
		}

		private static void IspisiNastavnikaPoIdentifikatoru(int idNastavnika, List<Nastavnici> nastavnici)
		{
			for (int i = 0; i < nastavnici.Count; i++)
			{
				if ((int)nastavnici[i].NastavnikId == idNastavnika)
				{
					Console.WriteLine($"id[{nastavnici[i].NastavnikId}] Nastavnik {nastavnici[i].Ime,-15} {nastavnici[i].Prezime,-15} je u zvanju {nastavnici[i].Zvanje}");
				}

			}
		}

		// Metode za predmet
		private static void UcitajPredmete(string predmetiTxt, List<Predmeti> predmeti)
		{
			string[] pred = predmetiTxt.Split('\n');

			// 0 - identifikatori, 1 - nazivi

			for (int i = 0; i < pred.Length; i++)
			{
				string[] podaciPredmeta = pred[i].Split(',');
				predmeti.Add(new Predmeti(int.Parse(podaciPredmeta[0]), podaciPredmeta[1]));

			}
		}
		private static void IspisiPredmete(List<Predmeti> predmeti)
		{
			Console.WriteLine("Predmeti:");
			for (int i = 0; i < predmeti.Count; i++)
			{
				Console.WriteLine($"id[{predmeti[i].PredmetId}] Predmet {predmeti[i].Naziv}");
			}
		}

		private static void IspisiPredmetePoIdentifikatoru(int idPredmeta, List<Predmeti> predmeti)
		{
			for (int i = 0; i < predmeti.Count; i++)
			{
				if ((int)predmeti[i].PredmetId == idPredmeta)
				{
					Console.WriteLine("id[{0}] Predmet {1}", predmeti[i].PredmetId, predmeti[i].Naziv);
				}

			}
		}

		// Metode za studente
		private static void UcitajStudente(string studentiTxt, List<Studenti> studenti)
		{
			string[] stud = studentiTxt.Split('\n');

			// 0 - identifikatori, 1 - indeksi, 2 - imena, 3 - prezimena, 4 - mesta
			// 1,E1 01/2016,Jovanović,Zarko,Loznica

			for (int i = 0; i < stud.Length; i++)
			{
				string[] podaciStudenata = stud[i].Split(',');

				studenti.Add(new Studenti(Int32.Parse(podaciStudenata[0]), podaciStudenata[1], podaciStudenata[2], podaciStudenata[3], podaciStudenata[4]));
			}
		}
		private static void IspisiStudente(List<Studenti> studenti)
		{
			Console.WriteLine("Studenti:");
			for (int i = 0; i < studenti.Count ; i++)
			{
				Console.WriteLine($"id[{studenti[i].StudentId}] Student {studenti[i].Index,-12} {studenti[i].Ime,-15} {studenti[i].Prezime,-15} je iz {studenti[i].Grad}");
			}
		}

		private static void IspisiStudenteSaSmera(List<Studenti> studenti, string smer)
		{
			Console.WriteLine("Studenti sa smera {0} su: ", smer);
			for (int i = 0; i < studenti.Count; i++)
			{
				if (((string)studenti[i].Index).ToLower().Contains((string)smer.ToLower()))
				{
					Console.WriteLine($"id[{studenti[i].StudentId}] Student {studenti[i].Index,-12} {studenti[i].Ime,-15} {studenti[i].Prezime,-15} je iz {studenti[i].Grad}");
				}

			}
		}

		private static void IspisiStudentePoIdentifikatoru(int idStudenta, List<Studenti> studenti)
		{
			for (int i = 0; i < studenti.Count; i++)
			{
				if ((int)studenti[i].StudentId == idStudenta)
				{
					Console.WriteLine($"id[{studenti[i].StudentId}] Student {studenti[i].Index,-12} {studenti[i].Ime,-15} {studenti[i].Prezime,-15} je iz {studenti[i].Grad}");
				}

			}
		}

		private static void IspisiStatistikuUpisa(List<Studenti> studenti)
		{
			int ukupno2016 = 0, ukupno2015 = 0, ukupno2014 = 0, ukupno2013 = 0, ukupno2012 = 0;

			for (int i = 0; i < studenti.Count; i++)
			{
				int godina = Int32.Parse(((string)studenti[i].Index).Split(new char[] { '/' })[1]);
				switch (godina)
				{
					case 2016:
						ukupno2016++;
						break;
					case 2015:
						ukupno2015++;
						break;
					case 2014:
						ukupno2014++;
						break;
					case 2013:
						ukupno2013++;
						break;
					case 2012:
						ukupno2012++;
						break;
					default:
						break;
				}
			}

			Console.WriteLine("Statistika upisa: \n***************************************");
			Console.WriteLine(ukupno2016 > 0 ? "U 2016 je upisalo fakultet " + ukupno2016 + " studenta" : "U 2016 nije bilo upisanih studenata");
			Console.WriteLine(ukupno2015 > 0 ? "U 2015 je upisalo fakultet " + ukupno2015 + " studenta" : "U 2015 nije bilo upisanih studenata");
			Console.WriteLine(ukupno2014 > 0 ? "U 2014 je upisalo fakultet " + ukupno2014 + " studenta" : "U 2014 nije bilo upisanih studenata");
			Console.WriteLine(ukupno2013 > 0 ? "U 2013 je upisalo fakultet " + ukupno2013 + " studenta" : "U 2013 nije bilo upisanih studenata");
			Console.WriteLine(ukupno2012 > 0 ? "U 2012 je upisalo fakultet " + ukupno2012 + " studenta" : "U 2012 nije bilo upisanih studenata");
			Console.WriteLine("***************************************");
		}



	}
}
