﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Racun.src
{
	class Zadatak1
	/*
	Napraviti Visual Studio projekat koji sadrži C# program za evidenciju kupljenih artikala I omogućuje prikaz fiskalnog računa.
	Račun je potrebno da sadrži:
		1. naziv prodavnice
		2. stavke računa
		3. ukupna cena bez pdv-a
		4. ukupna cena sa pdv-om (23%)
	Stavka računa je definisana nazivom artikla, količinom I cenom po komadu.Ukupna cena se računa a
	ostale vrednosti skladište u nizovima.
	Naziv prodavnice se prosleđuje putem argumenata komandne linije. Ostale podatke inicijalizovati testnim vrednostima.
	*/

	/*
	 Zadatak 3 :
		Proširiti zadatak 1 tako da postoji i informacija o tome u čemu je izražena količina (kilogramima,
		litrima, komadima). Određeni artikli mogu biti na akciji od 10%. Takođe, određeni proizvodi imaju
		10% ili 20% pdv-a, dok postoje i oni proizvodi koji su oslobođeni od pdv-a.
		Prilikom obračuna ukupne cene (bez pdv-a i sa pdv-om) voditi računa o akcijama i različitim stopama
		pdv-a.
	*/
	{
		static void Main(string[] args)
		{
			string NazivArtikla;
			int Kolicina;
			double CenaPoKomadu;
			string[] NizStavkaRacuna = new string[3];
			string StavkeRacuna = "1,Plazma Keks, 4, 105;2,Patike Air Max, 1, 12500;3,Luster, 2, 1276";
			string[] rac1 = StavkeRacuna.Split(';');

			for (int i = 0; i < rac1.Length; i++)
			{
				//rac[o] = Plazma Keks, 4, 105.00
				Console.WriteLine(args[i]);
				string[] racun = rac1[i].Split(',');

				NazivArtikla = racun[1];
				Kolicina = int.Parse(racun[2]);
				CenaPoKomadu = double.Parse(racun[3]);
				double UkupnaCena = Kolicina * CenaPoKomadu;
				double UkupnaCenaSaPDV = UkupnaCena * 1.23;
				PrikaziRacun(NazivArtikla, Kolicina, UkupnaCenaSaPDV);
				NizStavkaRacuna[i] = DodajUNiz(NazivArtikla, Kolicina, CenaPoKomadu);
			}

			foreach (string str in NizStavkaRacuna)
			{
				Console.WriteLine(str);
			}

			Console.ReadKey();
		}

		static void PrikaziRacun(string artikal, int kolicina, double cenaPDV)
		{
			Console.WriteLine("Naziv artikla: " + artikal + "\nkomada: " + kolicina + "\ncena sa PDV-om: {0:0 din} \n", cenaPDV);
		}

		static string DodajUNiz(string artikal, int kolicina, double cena)
		{
			string racun = artikal + ", kolicina: " + kolicina + ", cena po komadu: " + cena + "din.";
			return racun;
		}
	}
}
