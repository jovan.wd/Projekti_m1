﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Racun.src
{
	/*
	 Zadatak 7
		Sortirati karaktere stringa “RWRzBRAaBWARwBZ“ u rastućem (“AABBBRRRRWWZawz“) i
		opadajućem (“zwaZWWRRRRBBBAA“) redosledu (zasebno) upotrebom for ili while petlji. Koristiti
		selection sort algoritam za sortiranje, a dodatno probati još neki drugi.
		
		Pomoć: Od datog stringa kreirati niz karatkera i zatim uz pomoć dve petlje porediti vrednosti
		karaktera i menjati njihova “mesta”. Za zamenu dve vrednosti niza (A i B) koristiti dodatnu pomoćnu
		promenljivu (npr. tmp = A, A = B, B = tmp).
	*/
	class Sortiranje
	{
		static void Main(string[] args)
		{
			string str = "RWRzBRAaBWARwBZ";
			#region Array.Sort
			Console.WriteLine("Before sorting: \n" + str);
			Console.WriteLine("After sorting with Array.Sort");
			SortArray(str);
			#endregion

			char[] niz = str.ToCharArray();
			for (int j = 1; j < niz.Length; j++)
			{
				for (int i = 0; i < niz.Length - 1; i++)
				{
					if (niz[i].CompareTo(niz[i + 1]) > 0)
					{
						char temp = niz[i];
						niz[i] = niz[i + 1];
						niz[i + 1] = temp;
					}
				}
			}
			Console.WriteLine("\nSorting with Sort Algorithm:\n");
			foreach(char c in niz)
				Console.Write(c);
			Console.WriteLine("\n");
			//****************************************************************

			for (int j =1; j < niz.Length; j++)
			{
				for (int i = 0; i <niz.Length-1; i++)
				{
					if (niz[i].CompareTo(niz[i + 1]) < 0)
					{
						char temp = niz[i];
						niz[i] = niz[i + 1];
						niz[i + 1] = temp;
					}
				}
			}
			Console.WriteLine("***************************************************\nSorting reverse with Sort Algorithm:\n");
			foreach (char c in niz)
				Console.Write(c);

			Console.ReadKey();
		}

		public static void SortArray(string s)
		{
			char[] nizKaraktera = s.ToCharArray();
			char[] obrnut = nizKaraktera;
			Array.Sort(nizKaraktera);
			Console.WriteLine(nizKaraktera);
		}
	}
}
