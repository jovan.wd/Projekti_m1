﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Racun.src
{
	//Napisati C# program koji omogućuje prikaz određenih entiteta Studentse Službe. Model je dat u
	//posebnom fajlu studentska_sluzba_dijagram.pdf(o detaljnom tumačenju slike više naredni čas).
	//Za entitete potrebno je definisati nizove čije će se vrednosti popuniti parsiranjem teksta.
	//Potrebno je za svaki tip entiteta napraviti funkcije:
	//	1. Učitaj podatke
	//	2. Ispiši sve podatke
	//	3. Ispiši podatak u odnosu na identifikator(predmet_id, nastavnik_id,...)
	//Entiteti nastavnika su opisani tekstom:
	//	"1,Petar,Petrović,Docent\n2,Jovan,Jovanović,Docent\n3,Marko,Marković,Asistent\n4,Nikola,Nikolić,
	//Redovni Profesor\n5,Lazar,Lazić,Asistent"
	//Entiteti predmeta su opisani tekstom:
	//	"1,Matematika\n2,Fizika\n3,Elektrotehnika\n4,Informatika"

	/*
	 Zadatak 4
		Proširiti zadatak 2, tako da se omogući rad sa studentima (učitaj, ispisi sve i ispisi u odnosu na
		identifikator). Podaci studenata su opisani tekstom:
		"1,E1 01/2016,Jovanović, Zarko,Loznica\n2,E2 02/2015,Prosinečki,Strahinja,Novi Sad\n3,E2
		33/2016,Savić,Nebojša,Inđija\n4,SW 36/2013,Sekulić,Ana,Niš\n5,E2 157/2013,Nedeljković,Vuk,Novi
		Sad\n6,E1 183/2013,Klainić,Jovana,Sombor\n7,E2 44/2015,Bojana,Panić,Sr. Mitrovica"

	 Zadatak 5
		Proširiti zadatak 4, tako da se kreira funkcija koja omogućuje ispis svih studenta koji dolaze sa smera
		E2 (Računarstvo i Automatika)

	 Zadatak 6
		Proširiti zadatak 5, tako da se kreira funkcija koja omogućuje ispis statistike broja upisanih studenata
		po godinama upisa.
		Ispis bi trebao da bude nalik:
		*********************************************************************************
		U 2016 je upisalo fakultet 2 studenta
		U 2015 je upisalo fakultet 2 studenta
	    U 2014 nije bilo upisanih studenta
		U 2013 je upisalo fakultet 3 studenta
		*********************************************************************************
		 */
	class Zadatak2StudentskaSluzba
	{
		#region MainMetoda
		static void Main(string[] args)
		{
			string nastavniciTXT = "1,Petar,Petrović,Docent\n" +
								"2,Jovan,Jovanović,Docent\n" +
								"3,Marko,Marković,Asistent\n" +
								"4,Nikola,Nikolić, Redovni Profesor\n" +
								"5,Lazar,Lazić,Asistent";

			string predmetiTXT = "1,Matematika\n" +
							  "2,Fizika\n" +
							  "3,Elektrotehnika\n" +
							  "4,Informatika";

			string studentiTXT = "1,E1 01/2016,Jovanović, Zarko,Loznica\n" +
								 "2,E2 02/2015,Prosinečki,Strahinja,Novi Sad\n" +
								 "3,E2 33/2016,Savić,Nebojša,Inđija\n" +
								 "4,SW 36/2013,Sekulić,Ana,Niš\n" +
								 "5,E2 157/2013,Nedeljković,Vuk,Novi Sad\n" +
								 "6,E1 183/2013,Klainić,Jovana,Sombor\n" +
								 "7,E2 44/2015,Bojana,Panić,Sr.Mitrovica";

			string[] nastavnici = UcitajPodatke(nastavniciTXT);
			string[] predmeti = UcitajPodatke(predmetiTXT);
			string[] studenti = UcitajPodatke(studentiTXT);
			Console.WriteLine("Nastavnici:\n");
			IspisiPodatke(nastavnici);
			Console.WriteLine("\nPredmeti:\n");
			IspisiPodatke(predmeti);
			Console.WriteLine("\nStudenti:\n");
			IspisiPodatke(studenti);
			Console.WriteLine("\n");

			// Ispis nastavnika sa IdNastavnika = 2;
			IspisiPodatkePoId(2, nastavnici);

			// Ispis predmeta sa IdPredmeta = 4;
			IspisiPodatkePoId(4, predmeti);

			// Ipisi Studenta sa zadatim IdStudenta = 6;
			IspisiPodatkePoId(6, studenti);

			// Ispis studenata sa E2 smera
			Console.WriteLine("\nStudenti sa smera E2(Racunarstvo i Automatika):\n");
			IspisStudenataSaE2(studenti);

			// Ipis po godini upisa
			IspisPoGodiniUpisa(studenti);
			Console.ReadKey();
		}
		#endregion

		#region Ucitavanje
		public static string[] UcitajPodatke(string strtxt)
		{
			string[] podaci = strtxt.Split('\n');
			return podaci;
		}
		#endregion

		#region Ispis podataka
		public static void IspisiPodatke(string[] podaci)
		{
			foreach (string str in podaci)
			{
				Console.WriteLine("\t" + str);
			}
		}

		public static void IspisiPodatkePoId(int ID, string[] podaci)
		{
			//int id = 2;

			//"1,Petar,Petrović,Docent" 
			//"2,Jovan,Jovanović,Docent" 
			//"3,Marko,Marković,Asistent" 
			//"4,Nikola,Nikolić, Redovni Profesor" 
			//"5,Lazar,Lazić,Asistent";
			for (int i = 0; i < podaci.Length; i++)
			{
				string[] parsiraj = podaci[i].Split(',');
				if (ID == int.Parse(parsiraj[0]))
					Console.WriteLine(podaci[i]);
			}
		}
		#endregion

		public static void IspisStudenataSaE2(string[] studenti)
		{
			//7,E2 44/2015,Bojana,Panić,Sr. Mitrovica
			for (int i = 0; i < studenti.Length; i++)
			{
				string[] student = studenti[i].Split(',');
				if (student[1].Contains("E2"))
					Console.WriteLine("\t" + studenti[i]);
			}
		}

		public static void IspisPoGodiniUpisa(string[] studenti)
		{
			int Upis2015 = 0; int Upis2014 = 0; int Upis2013 = 0; int Upis2016 = 0;
			//7,E2 44/2015,Bojana,Panić,Sr. Mitrovica
			for (int i = 0; i < studenti.Length; i++)
			{
				string[] student = studenti[i].Split(',');
				if (student[1].Contains("2016")) Upis2016 += 1;
				else if (student[1].Contains("2015")) Upis2015 += 1;
				else if (student[1].Contains("2014")) Upis2014 += 1;
				else if (student[1].Contains("2013")) Upis2013 += 1;
			}
			Console.WriteLine("\n******************************************************");
			if (Upis2016 == 0) Console.WriteLine("U 2016 nije bilo upisanih studenta");
			else Console.WriteLine("U 2016 je upisalo fakultet {0} studenta", Upis2016);
			if (Upis2015 == 0) Console.WriteLine("U 2015 nije bilo upisanih studenta");
			else Console.WriteLine("U 2015 je upisalo fakultet {0} studenta", Upis2015);
			if (Upis2014 == 0) Console.WriteLine("U 2014 nije bilo upisanih studenta");
			else Console.WriteLine("U 2014 je upisalo fakultet {0} studenta", Upis2014);
			if (Upis2013 == 0) Console.WriteLine("U 2013 nije bilo upisanih studenta");
			Console.WriteLine("U 2013 je upisalo fakultet {0} studenta", Upis2013);
			Console.WriteLine("******************************************************");
		}
	}
}
