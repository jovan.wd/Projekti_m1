﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Knjizara
{
	class Recnik:Knjiga
	{
		public int BrojReci;
		public string IzvorniJezik;
		public string CiljniJezik;

		public Recnik(string naziv, int broj, int brojreci, string izvorni, string ciljni): base(naziv, broj)
		{
			Naziv = naziv;
			BrojStrana = broj;
			BrojReci = brojreci;
			IzvorniJezik = izvorni;
			CiljniJezik = ciljni;
		}
		public Recnik()
		{

		}
		public override string ToString()
		{
			return base.ToString() + " broj reci: " + BrojReci + ", izvorni jezik: " + IzvorniJezik + ", ciljni jezik: " + CiljniJezik; ;
		}
	}
}
