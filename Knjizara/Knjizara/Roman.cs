﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Knjizara
{
	class Roman:Knjiga
	{
		public string Pisac;

		public Roman(string naziv, int broj, string pisac):base(naziv, broj)
		{
			Naziv = naziv;
			BrojStrana = broj;
			Pisac = pisac;
		}
		public Roman()
		{

		}
		public override string ToString()
		{
			return base.ToString()+ ", pisac: " + Pisac;
		}
	}
}
