﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Knjizara
{
	class Knjiga
	{
		public string Naziv;
		public int BrojStrana;

		public Knjiga()
		{

		}
		public Knjiga(string naziv, int broj)
		{
			Naziv = naziv;
			BrojStrana = broj;
		}
		public override string ToString()
		{
			return Naziv.ToUpper() + ", broj strana: " + BrojStrana; ;
		}
	}
}
