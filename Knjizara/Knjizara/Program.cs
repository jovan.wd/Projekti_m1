﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace Knjizara
{
	class Program
	{

		static void Main(string[] args)
		{
			List<Knjiga> sveKnjige = new List<Knjiga>();
			XmlDocument doc = new XmlDocument();

			string trenutnaPutanja = Directory.GetCurrentDirectory();
			string putanjaProjekta = new DirectoryInfo(trenutnaPutanja).Parent.Parent.FullName;
			
			string putanjaXMLFajla = putanjaProjekta + "\\data\\knjizara_ponuda.xml";
			
			try
			{
				doc.Load(putanjaXMLFajla);
			}
			catch (Exception e)
			{
				// Ukoliko je doslo do greske, ispisi izuzetak i prekini aplikaciju
				Console.WriteLine(e.ToString());
				Console.ReadLine();
				Environment.Exit(-1);
			}

			// Preuzimamo prvi nod naseg XML dokumenta, sto bi trebalo da bude 'PONUDA'
			XmlNode knjiga = doc.FirstChild;

			// Preuzimamo listu dece noda 'PONUDA', sto bi trebalo da bude lista ROMAN/RECNIK-a
			XmlNodeList roman_recnik = knjiga.ChildNodes;

			// Iteriramo kroz svaku knjigu u listi
			Knjiga k = null;

			foreach (XmlNode rr in roman_recnik)
			{
				if (rr.Name == "RECNIK")
					k = new Recnik(rr.ChildNodes[0].InnerText, int.Parse(rr.ChildNodes[1].InnerText), int.Parse(rr.ChildNodes[2].InnerText), rr.ChildNodes[3].InnerText, rr.ChildNodes[4].InnerText);
				else
					k = new Roman(rr.ChildNodes[0].InnerText, int.Parse(rr.ChildNodes[1].InnerText), rr.ChildNodes[2].InnerText);
				sveKnjige.Add(k);
			}

			foreach (Knjiga book in sveKnjige)
			{
				Console.WriteLine(book);
			}
			Console.ReadLine();
		}
	}
}