use DotNetKurs

create table Karta
(	
	kartaID int not null primary key identity,
	nazivDogadjaja nvarchar(50),
	tipKarte nvarchar(50),
	brojReda int,
	brojSedista int,
	sektor nvarchar(5),
	cena int
)

 create table Rezervacija
(	
	rezervacijID int not null primary key identity,
	kartaID int unique,
	imeGosta nvarchar(50),
	prezimeGosta nvarchar(50),
	foreign key(kartaID) references Karta(kartaID)
)

insert into Karta(nazivDogadjaja, tipKarte, brojReda, brojSedista, sektor, cena)
values
	('JAY LUMEN - GREEN LOVE', 'VIP', 1, 2, 'IX', 3990),
	('JAY LUMEN - GREEN LOVE', 'tribina', 12, 55, 'II', 1990),
	('NS KONCERT GODINE 2017', 'tribina', 12, 5, 'IV', 2090),
	('NERVOZNI PO�TAR', 'tribina', 10, 15, 'VI', 990);

insert into Rezervacija(kartaID, imeGosta, prezimeGosta)
values
	(1, 'Jovan', 'Jovanovic'),
	(2, 'Nevena', 'Bozovic'),
	(3, 'Milan', 'Novakovic'),
	(4, 'Sanja', 'Ilic')