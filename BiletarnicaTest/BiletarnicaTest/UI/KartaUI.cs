﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BiletarnicaTest
{
	class KartaUI
	{

		public static void IspisiSveKarte()
		{
			Console.Clear();
			Console.WriteLine("Karte[id, naziv dogadjaja, tip karte, broj reda, broj sedista, sektro, cena]:");
			List<Karta> sveKarte = KartaDAO.DajSveKarte(Program.conn);
			for (int i = 0; i < sveKarte.Count; i++)
			{
				Console.WriteLine(sveKarte[i]);
			}
			Console.WriteLine();
		}
		public static void KreirajNovuKartu()
		{
			Console.Write("Unesi naziv dogadjaja:");
			string kartaNaziv = PomocnaKlasa.OcitajTekst();
			kartaNaziv= kartaNaziv.ToUpper();
			Console.Write("Unesi tip karte:");
			string tipKarte = PomocnaKlasa.OcitajTekst();
			Console.Write("Unesi broj reda:");
			int brReda = PomocnaKlasa.OcitajCeoBroj();
			Console.Write("Unesi broj sedista:");
			int brojSedista = PomocnaKlasa.OcitajCeoBroj();
			Console.Write("Unesi sektor: ");
			string sektor = PomocnaKlasa.OcitajTekst();
			int cena = PomocnaKlasa.OcitajCeoBroj();

			Karta karta = new Karta(kartaNaziv, tipKarte, brReda, brojSedista, sektor, cena);

			KartaDAO.Add(Program.conn, karta);
		}
	}
}
