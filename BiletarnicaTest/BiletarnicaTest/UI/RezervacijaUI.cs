﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BiletarnicaTest
{
	class RezervacijaUI
	{
		public static void IspisiRezervacijuPoImenuGosta()
		{
			Rezervacija rez = null;
			Console.Write("Unesite ime gosta ciju rezervaciju zelite da vidite: ");
			string imeGosta = PomocnaKlasa.OcitajTekst();
			rez = RezervacijaDAO.PregledRezervacijePoImenuGosta(Program.conn, imeGosta);
			Console.WriteLine(rez);
		}

		public static void IspisiSveRezervacije()
		{
			List<Rezervacija> lista = RezervacijaDAO.DodajSveRezervacijeUListu(Program.conn);
			foreach (Rezervacija rez in lista)
			{
				Console.WriteLine(rez);
			}
		}
		public static void KreirajNovuRez()
		{
			Console.Clear();
			NoviUnos: Console.WriteLine("Novi unos:");
			Rezervacija rez = null;
			Console.Write("Unesite ime: ");
			string ime = PomocnaKlasa.OcitajTekst();
			Console.Write("Unesite prezime: ");
			string prezime = PomocnaKlasa.OcitajTekst();
			Console.Write("Izaberite dogadjaj za koji hocete kartu: ");
			string nazivDogadjaja = PomocnaKlasa.OcitajTekst().ToUpper();
			Console.Write("Izaberite tip karte(vip, parter, tribina): ");
			string tip = PomocnaKlasa.OcitajTekst();
			Console.Write("Unesite koji red zelite: ");
			int red = PomocnaKlasa.OcitajCeoBroj();
			Console.Write("Unesite koji sediste zelite: ");
			int sediste = PomocnaKlasa.OcitajCeoBroj();
			Console.Write("Izaberite sektor(I-X): ");
			string sektor = PomocnaKlasa.OcitajTekst();

			//da li karta postoji
			bool postoji = DaliKartaPostoji(nazivDogadjaja, tip, red, sediste, sektor);
			if (postoji==true)
			{
				Console.WriteLine("Karta postoji, pritisnite bilo sta za novi unos:");

				Console.ReadKey();
				goto NoviUnos;
			}
			int cena = 0;
			if (nazivDogadjaja == "JAY LUMEN - GREEN LOVE" && tip == "VIP") cena += 3990;
			else if (nazivDogadjaja == "JAY LUMEN - GREEN LOVE" && tip == "tribina") cena += 1990;
			else if (nazivDogadjaja == "JAY LUMEN - GREEN LOVE" && tip == "parter") cena += 2990;

			if (nazivDogadjaja == "NS KONCERT GODINE 2017" && tip == "VIP") cena += 3090;
			else if (nazivDogadjaja == "NS KONCERT GODINE 2017" && tip == "tribina") cena += 1090;
			else if (nazivDogadjaja == "NS KONCERT GODINE 2017" && tip == "parter") cena += 2090;

			if (nazivDogadjaja == "NERVOZNI POŠTAR" && tip == "VIP") cena += 2990;
			else if (nazivDogadjaja == "NERVOZNI POŠTAR" && tip == "tribina") cena += 990;
			else if (nazivDogadjaja == "NERVOZNI POŠTAR" && tip == "parter") cena += 1990;

			Karta karta = new Karta(nazivDogadjaja, tip, red, sediste, sektor, cena);
			rez = new Rezervacija(karta, ime, prezime);
			
			RezervacijaDAO.KreiranjeNoveRezervacije(Program.conn, rez);
		}
		public static bool DaliKartaPostoji(string nazivDogadjaja, string tip, int red, int sediste, string sektor)
		{
			bool postoji = false;
			List<Karta> lista = KartaDAO.DajSveKarte(Program.conn);
			foreach (Karta k in lista)
			{
				if (k.NazivDogadjaja == nazivDogadjaja && k.TipKarte == tip && k.BrojReda == red && k.BrojSedista == sediste && k.Sektor == sektor)
				{
					postoji = true;
				}
			}
			return postoji;
		}
	}
}
