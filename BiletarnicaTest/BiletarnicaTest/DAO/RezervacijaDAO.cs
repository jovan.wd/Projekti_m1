﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BiletarnicaTest
{
	class RezervacijaDAO
	{
		private static Karta karta;
		

		/*Kreiranje nove rezervacije gde korisnik unosi: ime, prezime, odabir karte, red,
		sedište i sektor.Imati u vidu da karta ne može biti dva puta rezervisana.*/
		public static bool KreiranjeNoveRezervacije(SqlConnection conn, Rezervacija rez)
		{
			bool retVal = false;
			try
			{
				//dodamo kartu u bazu
				KartaDAO.Add(Program.conn, rez.Karta);
				//vratimo njen ID da bi je ubacili u rezervacije
				Karta value = KartaDAO.PronadjiKartu(Program.conn, rez.Karta);
				int idkarte = value.KartaID;

				string updateRez = "INSERT INTO Rezervacija(kartaID, imeGosta, prezimeGosta) values (@kartaID, @imeGosta, @prezimeGosta)";
				SqlCommand cmd = new SqlCommand(updateRez, conn);
				cmd.Parameters.AddWithValue("@kartaID", idkarte);
				cmd.Parameters.AddWithValue("@imeGosta", rez.ImeGosta);
				cmd.Parameters.AddWithValue("@prezimeGosta", rez.PrezimeGosta);
				if (cmd.ExecuteNonQuery() == 1)
				{
					retVal = true;
				}
			}
			catch (Exception e)
			{
				Console.WriteLine(e.ToString());
			}
			return retVal;
		}

		public static List<Rezervacija> DodajSveRezervacijeUListu(SqlConnection conn)
		{
			List<Rezervacija> sveRezervacije = new List<Rezervacija>();
			try
			{
				string query = "select rezervacijID, Karta.kartaID, nazivDogadjaja, tipKarte, brojReda, brojSedista, sektor, cena, imeGosta, prezimeGosta " +
							   "from Rezervacija inner join Karta on Rezervacija.kartaID = Karta.kartaID";
				SqlCommand cmd = new SqlCommand(query, conn);
				SqlDataReader rdr = cmd.ExecuteReader();
				while (rdr.Read())
				{
					int id = (int)rdr["rezervacijID"];
					int kartaID = (int)(rdr["kartaID"]);
					string dogadjaj = (string)rdr["nazivDogadjaja"];
					string tipKarte = (string)rdr["tipKarte"];
					int brojReda = (int)rdr["brojReda"];
					int brojSedista = (int)rdr["brojSedista"];
					string sektor = (string)rdr["sektor"];
					int cena = (int)rdr["cena"];
					karta = new Karta(kartaID, dogadjaj, tipKarte, brojReda, brojSedista, sektor, cena);
					string imeGosta = (string)rdr["imeGosta"];
					string prezimeGosta = (string)rdr["prezimeGosta"];
					sveRezervacije.Add(new Rezervacija(id, karta, imeGosta, prezimeGosta));
				}
				rdr.Close();
			}
			catch (Exception e)
			{
				Console.WriteLine(e.ToString());
			}
			return sveRezervacije;
		}
		public static Rezervacija PregledRezervacijePoImenuGosta(SqlConnection conn, string imeGosta)
		{
			Rezervacija rezervacija = null;
			try
			{
				string query = "select Karta.kartaID, nazivDogadjaja, tipKarte, brojReda, brojSedista, sektor, cena, rezervacijID, prezimeGosta from Karta join Rezervacija " +
							   "on Karta.kartaID = Rezervacija.kartaID where Rezervacija.imeGosta = @imeGosta";
				SqlCommand cmd = new SqlCommand(query, conn);
				cmd.Parameters.AddWithValue("@imeGosta", imeGosta);
				SqlDataReader rdr = cmd.ExecuteReader();
				if( rdr.Read())
				{
					int kartaID = (int)rdr["kartaID"];
					string dogadjaj = (string)rdr["nazivDogadjaja"];
					string tipKarte = (string)rdr["tipKarte"];
					int brojReda = (int)rdr["brojReda"];
					int brojSedista = (int)rdr["brojSedista"];
					string sektor = (string)rdr["sektor"];
					int cena = (int)rdr["cena"];

					Karta karta = new Karta(kartaID, dogadjaj, tipKarte, brojReda, brojSedista, sektor, cena);

					int rezervacijaID = (int)rdr["rezervacijID"];
					string prezimeGosta = (string)rdr["prezimeGosta"];
					rezervacija = new Rezervacija(rezervacijaID, karta, imeGosta, prezimeGosta);
				}
				rdr.Close();
			}
			catch (Exception e)
			{
				Console.WriteLine(e.ToString());
			}
			return rezervacija;
		}
	}
}
