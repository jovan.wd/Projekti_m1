﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BiletarnicaTest
{
	class KartaDAO
	{
		public static Karta PronadjiKartu(SqlConnection conn, Karta k)
		{
			Karta karta = null;
			try
			{
				string query = "select kartaID, nazivDogadjaja, tipKarte, brojReda, BrojSedista, sektor, cena from Karta where  nazivDogadjaja=@nazivDogadjaja and tipKarte= @tipKarte and brojReda = @brojReda and brojSedista=@brojSedista and sektor=@sektor and cena=@cena";
				
				SqlCommand cmd = new SqlCommand(query, conn);
				cmd.Parameters.AddWithValue("@nazivDogadjaja", k.NazivDogadjaja);
				cmd.Parameters.AddWithValue("@tipKarte", k.TipKarte);
				cmd.Parameters.AddWithValue("@brojReda", k.BrojReda);
				cmd.Parameters.AddWithValue("@brojSedista", k.BrojSedista);
				cmd.Parameters.AddWithValue("@sektor", k.Sektor);
				cmd.Parameters.AddWithValue("@cena", k.Cena);
				SqlDataReader rdr = cmd.ExecuteReader();

				if (rdr.Read())
				{
					int kartaID = (int)rdr["kartaID"];
					string naziv = (string)rdr["nazivDogadjaja"];
					string tipKarte = (string)rdr["tipKarte"];
					int brojReda = (int)rdr["brojReda"];
					int brojSedista = (int)rdr["brojSedista"];
					string sektor = (string)rdr["sektor"];
					int cena = (int)rdr["cena"];
					karta = new Karta(kartaID, naziv, tipKarte, brojReda, brojSedista, sektor, cena);
				}
				rdr.Close();
			}
			catch (Exception e)
			{
				Console.WriteLine(e.ToString());
			}
			return karta;
		}

		// Trazimo sve karte kojr postoje u bazi podataka
		public static List<Karta> DajSveKarte(SqlConnection conn)
		{
			List<Karta> listaKarata = new List<Karta>();
			try
			{
				string query = "SELECT kartaID, nazivDogadjaja, tipKarte, brojReda, brojSedista, sektor, cena FROM Karta";
				SqlCommand cmd = new SqlCommand(query, conn);
				SqlDataReader rdr = cmd.ExecuteReader();
				while (rdr.Read())
				{
					int id = (int)rdr["kartaID"];
					string dogadjaj  = (string)rdr["nazivDogadjaja"];
					string tipKarte = (string)rdr["tipKarte"];
					int brojReda = (int)rdr["brojReda"];
					int brojSedista = (int)rdr["brojSedista"];
					string sektor = (string)rdr["sektor"];
					int cena =(int)(rdr["cena"]);

					Karta karta = new Karta(id, dogadjaj, tipKarte, brojReda, brojSedista, sektor, cena);
					
					listaKarata.Add(karta);
				}
				rdr.Close();

			}
			catch (Exception e)
			{
				Console.WriteLine(e.ToString());
			}
			return listaKarata;
		}

		// Ubacivanje novog studenta u bazu podataka
		public static bool Add(SqlConnection conn, Karta karta)
		{
			bool retVal = false;
			try
			{
				string update = "INSERT INTO Karta (nazivDogadjaja, tipKarte, brojReda, brojSedista, sektor, cena) values (@nazivDogadjaja, @tipKarte, @brojReda, @brojSedista, @sektor, @cena)";
				SqlCommand cmd = new SqlCommand(update, conn);

				cmd.Parameters.AddWithValue("@nazivDogadjaja", karta.NazivDogadjaja);
				cmd.Parameters.AddWithValue("@tipKarte", karta.TipKarte);
				cmd.Parameters.AddWithValue("@brojReda", karta.BrojReda);
				cmd.Parameters.AddWithValue("@brojSedista", karta.BrojSedista);
				cmd.Parameters.AddWithValue("@sektor", karta.Sektor);
				cmd.Parameters.AddWithValue("@cena", karta.Cena);

				if (cmd.ExecuteNonQuery() == 1)
				{
					retVal = true;
				}
			}
			catch (Exception e)
			{
				Console.WriteLine(e.ToString());
			}
			return retVal;
		}
	}
}
