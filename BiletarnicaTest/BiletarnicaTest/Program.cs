﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BiletarnicaTest
{
	class Program
	{
		public static SqlConnection conn;
		static void LoadConnection()
		{
			try
			{
				string konekcija = "Data Source=.\\SQLEXPRESS;Initial Catalog=DotNetKurs;Integrated Security=True;MultipleActiveResultSets=True";
				// Parametar "MultipleActiveResultSets=True" je neophodan kada zelimo da imamo istovremeno
				// otvorena dva data readera ka bazi podataka. Zasto je u ovom programu to neophodno?
				conn = new SqlConnection(konekcija);
				conn.Open();
			}
			catch (Exception e)
			{
				Console.WriteLine("Pogresna konekcija ka bazi!\n", e.ToString());
			}
		}
		public static void IspisiMenu()
		{
			Console.WriteLine("Biletarnica - Osnovne opcije:");
			Console.WriteLine("\tOpcija broj 1 - Pregled svih karata");
			Console.WriteLine("\tOpcija broj 2 - Ispisi odredjenu rezervaciju");
			Console.WriteLine("\tOpcija broj 3 - Ispisi sve rezervacije");
			Console.WriteLine("\tOpcija broj 4 - Kreiranje nove rezervacije");
			Console.WriteLine("\t\t ...");
			Console.WriteLine("\tOpcija broj 0 - IZLAZ IZ PROGRAMA");
		}
		static void Main(string[] args)
		{
			LoadConnection();
			int odluka = -1;
			while (odluka != 0)
			{
				IspisiMenu();
				Console.Write("opcija:");
				odluka = PomocnaKlasa.OcitajCeoBroj();
				switch (odluka)
				{
					case 0:
						Console.WriteLine("Izlaz iz programa");
						break;
					case 1:
						KartaUI.IspisiSveKarte();
						break;
					case 2:
						RezervacijaUI.IspisiRezervacijuPoImenuGosta();
						break;
					case 3:
						RezervacijaUI.IspisiSveRezervacije();
						break;
					case 4:
						RezervacijaUI.KreirajNovuRez();
						break;
					default:
						Console.WriteLine("Nepostojeca komanda");
						break;
				}
			}
		}
	}
}
