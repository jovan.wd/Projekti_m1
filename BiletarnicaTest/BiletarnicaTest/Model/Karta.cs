﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BiletarnicaTest
{
	class Karta
	{
		public int KartaID { get; set; }
		public string NazivDogadjaja { get; set; }
		public string TipKarte { get; set; }
		public int BrojReda { get; set; }
		public int BrojSedista { get; set; }
		public string Sektor { get; set; }
		public int Cena { get; set; }

		public Karta()
		{
			KartaID=0;
			NazivDogadjaja = "";
			TipKarte = "";
			BrojReda = 0;
			BrojSedista = 0;
			Sektor = "";
			Cena = 0;
		}

		public Karta(string naziv, int red, int sediste, string sektor)
		{
			NazivDogadjaja = naziv;
			BrojReda = red;
			BrojSedista = sediste;
			Sektor = sektor;
		}

		public Karta(string naziv, string tip, int red, int sediste, string sektor, int cena)
		{
			NazivDogadjaja = naziv;
			TipKarte = tip;
			BrojReda = red;
			BrojSedista = sediste;
			Sektor = sektor;
			Cena = cena;
		}
		public Karta(int id, string naziv, string tip, int red, int sediste, string sektor, int cena)
		{
			KartaID = id;
			NazivDogadjaja = naziv;
			TipKarte = tip;
			BrojReda = red;
			BrojSedista = sediste;
			Sektor = sektor;
			Cena = cena;
		}
		public override string ToString()
		{
			return KartaID + " * "+ NazivDogadjaja + " * " + TipKarte + " * " + BrojReda + " * " + BrojSedista + " * " + Sektor + " * " + Cena + "din.";
		}
		public string ToStringBezKartaID()
		{
			return NazivDogadjaja + " * " + TipKarte + " * " + BrojReda + " * " + BrojSedista + " * " + Sektor + " * " + Cena + "din.";
		}

	}
}
