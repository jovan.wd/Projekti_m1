﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BiletarnicaTest
{
	class Rezervacija
	{
		public int RezervacijaID { get; set; }
		public Karta Karta { get; set; }
		public string ImeGosta { get; set; }
		public string PrezimeGosta { get; set; }

		public Rezervacija(int id , Karta karta, string ime, string prezime)
		{
			RezervacijaID = id;
			Karta = karta;
			ImeGosta = ime;
			PrezimeGosta = prezime;
		}
		public Rezervacija(Karta karta, string ime, string prezime)
		{
			Karta = karta;
			ImeGosta = ime;
			PrezimeGosta = prezime;
		}
		public Rezervacija()
		{

		}
		public override string ToString()
		{
			return RezervacijaID +" * "+ Karta.ToStringBezKartaID()+" * "+ImeGosta +" * " + PrezimeGosta; 
		}
	}
}
