﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BiletarnicaTest
{
	class PomocnaKlasa
	{
		public static string OcitajTekst()
		{
			string tekst = ""; int broj=0;
			while (tekst == null || tekst.Equals("") || int.TryParse(tekst, out broj) == true)
				tekst = Console.ReadLine();

			return tekst;
		}
		
		public static int OcitajCeoBroj()
		{
			int ceoBroj = 0;
			string tekst;
			while (true)
			{
				tekst = Console.ReadLine();
				if (int.TryParse(tekst, out ceoBroj) == true)
				{
					break;
				}
			}
			return ceoBroj;
		}
	}
}
